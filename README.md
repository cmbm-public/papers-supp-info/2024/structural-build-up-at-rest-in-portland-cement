# Supplementary material

### Title
Structural build-up at rest in the induction and acceleration periods of Portland Cement

### Authors and affiliation
Luca Michel, Lex Reiter, Antoine Sanner, Robert J. Flatt, David S. Kammer

Institute of Building Materials, ETH Zurich, Switerland

### Date
2024/09/04

### Description
The repository contains the jupyter notebooks to reproduce the results and the figures as used in the paper.
The data used in the paper (and in the notebooks here) can be downloaded from the [ETH Research collection](https://www.research-collection.ethz.ch/).

#### Notebooks
- File **notebooks/figure_1.ipynb** reproduces the data used to identify 3 stages in structural build-up at rest (Figure 1, Section 1).
- File **notebooks/figure_3.ipynb** reproduces the amplitude sweeps to assess the linear elastic range (Figure 3, Section 2).
- File **notebooks/figure_4.ipynb** reproduces the coupling of storage modulus and heat data (Figure 4, Section 3.1).
- File **notebooks/figure_5.ipynb** reproduces the correspondence between 2 kinetic stages and one build-up regime (Figure 5, Section 3.4).
- File **notebooks/figure_6.ipynb** reproduces the assessement of initial noise in calorimetry data (Figure 6, Appendix A).
- File **notebooks/figure_7.ipynb** reproduces the exponential fits on the storage modulus vs. heat data (Figure 7, Appendix B).
- File **notebooks/figure_8.ipynb** reproduces the assessement of the reproducibility of the approach (Figure 8, Appendix C).
- File **notebooks/figure_9.ipynb** reproduces the dependence of the storage modulus at the onset on solid volume fraction (Figure 9, Appendix D).

### Usage/installation

The dependencies to run the notebooks are given in `requirements.txt`. These can be installed by running

```bash
pip install -r requirements.txt
```

To create a virtual environment

```bash
python -m venv name_of_venv

source name_of_venv/bin/activate
```