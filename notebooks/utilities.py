import matplotlib as mpl
import numpy as np


def set_plot_theme():
    """Set plot theme"""

    params = {
        "axes.labelsize": 7,
        "axes.titlesize": 6,
        "xtick.labelsize": 6,
        "ytick.labelsize": 6,
        "axes.titlepad": 1,
        "axes.labelpad": 1,
        "lines.linewidth": 0.8,
        "legend.fontsize": 5,
        "legend.title_fontsize": 6,
    }
    mpl.rcParams.update(params)


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


def find_closest_index(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx
